import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    public httpApi: HttpClient
  ) { }

  login(email: string, password: string): Observable<boolean> {
    return this.httpApi.post('https://reqres.in/api/login', {
      email,
      password
    }).pipe(
      tap((response: any) => localStorage.setItem('token', response?.token)),
      map((response: any) => !!response?.token),
      catchError(() => of(false))
    );
  }

  getUserList() {
    return this.httpApi.get('https://reqres.in/api/users?page=1').pipe(
      map((response: any) => response?.data)
    );
  }

  getUserDetail(id: number): Observable<any> {
    return this.httpApi.get(`https://reqres.in/api/users/${id}`).pipe(
      map((response: any) => response?.data)
    );
  }

  update(userId: number, userName: string, userJob: string): Observable<any> {
    let body = {
      "name": userName,
      "job": userJob
    }
    return this.httpApi.patch(`https://reqres.in/api/users/${userId}`, body).pipe(
      map((response: any) => response?.updatedAt)
    );
  }

  create(userFisrtname: string, userLastname: string, userEmail: string): Observable<any> {
    let body = {
      "firstname": userFisrtname,
      "lastname": userLastname,
      "email": userEmail
    }
    return this.httpApi.post(`https://reqres.in/api/users/`, body).pipe(
      map((response: any) => response?.createdAt)
    );
  }

  delete(userId: number): Observable<any> {
    return this.httpApi.delete(`https://reqres.in/api/users/${userId}`);
  }

  isMinPrice(price: number): boolean {
    const priceRef = 12;
    return price < priceRef;
  }

  logout() {
    localStorage.removeItem('token');
  }

  register(email: string, password: string): Observable<any> {
    const body = {
      "email": "eve.holt@reqres.in",  // "email": email,
      "password": "pistol"  // "password": password
    };
    return this.httpApi.post("https://reqres.in/api/register", body);
  }
}
