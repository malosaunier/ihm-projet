import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UserService } from '../api/user.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.scss'],
})
export class UserUpdateComponent implements OnInit, OnDestroy {
  
  createForm: FormGroup;

  userUpdate$: Subscription;

  @Input() user : any;

  // convenience getter for easy access to form fields
  get f() { return this.createForm.controls; }

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
    this.createForm = this.formBuilder.group({
      firstname: this.user.first_name,
      lastname: this.user.last_name,
      email: this.user.email
    });
  }

  ngOnDestroy() {
    if (this.userUpdate$) this.userUpdate$.unsubscribe();
  }

  onSubmit(data) {
    this.userUpdate$ = this.userService.update(this.user.id, data.firstname, data.lastname)
    .subscribe((res) => {  // Successful response from back end
      this.closeModal();
      alert("L'utilisateur a bien été modifié (" + res + ")");
    },
    (err) => {
      this.closeModal();
      alert("L'utilisateur n'a pas pu être modifié ! " + err);
    });
  }

  closeModal()
  {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

}
