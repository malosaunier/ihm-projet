import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserService } from '../api/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit, OnDestroy {

  email: string = "";
  password: string = "";
  password_conf: string = "";

  success_msg: string = "";
  err_msg: string = "";

  invalid: boolean = false;

  registerUser$: Subscription;

  constructor(
    public router: Router,
    private userService: UserService,
  ) { }

  ngOnInit() {

  }

  ngOnDestroy() {
    if (this.registerUser$) this.registerUser$.unsubscribe();
  }

  onCreatePress() {
    // Verify the password
    if (this.password != this.password_conf) {
      this.invalid = true;
    }

    this.registerUser$ = this.userService.register(this.email, this.password).subscribe(
      () => {
        this.success_msg = "Compte créé ! Bienvenu au club."
      },
      (err) => {
        console.error(err);
        this.err_msg = err.error.error;
      }
    )
  }
}
