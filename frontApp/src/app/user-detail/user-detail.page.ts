import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { UserService } from 'src/app/api/user.service';
import { createAnimation, Animation } from '@ionic/core'
import { ModalController } from '@ionic/angular';
import { UserUpdateComponent } from '../user-update/user-update.component';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.page.html',
  styleUrls: ['./user-detail.page.scss'],
})
export class UserDetailPage implements OnInit {
  private selectedUserId: number;

  userDetail$: Observable<any>;
  deleteUser$: Subscription;
  animation: Animation = createAnimation('');

  constructor(
    public userService: UserService,
    public actRoute: ActivatedRoute,
    public router: Router,
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
    this.selectedUserId = this.actRoute.snapshot.params.id;
    this.userDetail$ = this.userService.getUserDetail(this.selectedUserId);
  }

  async moveToUpdateModal(user: any){
    const modal = await this.modalCtrl.create({
      component: UserUpdateComponent,
      componentProps: { user },
      cssClass: 'my-custom-class'
    });

    return await modal.present();
  }

  deleteUser(userId: number){
    this.deleteUser$ = this.userService.delete(userId)
    .subscribe((res) => {
      alert("L'utilisateur est bien supprimé !");
    },
    (err) => {
      alert("L'utilisateur n'a pas pu être supprimé !");
    });
  }
}
