export interface User {
  email: any;
  id: number;
  firstName: string;
  lastName: string;
}
