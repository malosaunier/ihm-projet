import { ChangeDetectionStrategy, Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { UserUpdateComponent } from 'src/app/user-update/user-update.component';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/api/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserListComponent implements OnInit {

  deleteUser$: Subscription;

  @Input()
  userList: any;

  @Output()
  selectedUser = new EventEmitter<number>();

  constructor(
    private modalCtrl: ModalController,
    private userService: UserService
  ) { }

  async moveToUpdateModal(user: any){
    const modal = await this.modalCtrl.create({
      component: UserUpdateComponent,
      componentProps: { user },
      cssClass: 'my-custom-class'
    });

    return await modal.present();
  }

  deleteUser(userId: number){
    this.deleteUser$ = this.userService.delete(userId)
    .subscribe((res) => {
      alert("Utilisateur bien supprimé !");
    },
    (err) => {
      alert("Utilisateur non supprimé !");
    });
  }

  ngOnInit() {
  }

}
