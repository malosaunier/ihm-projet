import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UserService } from '../api/user.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss'],
})
export class UserCreateComponent implements OnInit, OnDestroy {
  
  createForm: FormGroup;

  userCreate$: Subscription;

  // convenience getter for easy access to form fields
  get f() { return this.createForm.controls; }

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    private modalCtrl: ModalController,
  ) { }

  ngOnInit() {
    this.createForm = this.formBuilder.group({
      firstname: '',
      lastname: '',
      email: ''
    });
  }

  ngOnDestroy() {
    if (this.userCreate$) this.userCreate$.unsubscribe();
  }

  onSubmit(data) {
   this.userCreate$ = this.userService.create(data.firstname, data.lastname, data.email)
    .subscribe((res) => {  // Successful response from back end
      this.closeModal();
      alert("L'utilisateur a bien été créé (" + res + ")");
    },
    (err) => {
      this.closeModal();
      alert("L'utilisateur n'a pas pu être créé ! " + err);
    });
  }

  closeModal()
  {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

}
