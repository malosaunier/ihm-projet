import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { UserService } from '../api/user.service';
import { UserCreateComponent } from '../user-create/user-create.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  userList$: Observable<any>;

  constructor(
    public router: Router,
    public userService: UserService,
    private modalCtrl: ModalController,
  ) {}

  async selectedUser(id: number) {
    await this.router.navigate(['user-detail', id]);
  }

  async moveToCreateModal(){
    const modal = await this.modalCtrl.create({
      component: UserCreateComponent,
      cssClass: 'my-custom-class'
    });

    return await modal.present();
  }

  ngOnInit(): void {
    this.userList$ = this.userService.getUserList();
  }

  async logout(){
    await this.userService.logout();
    await this.router.navigate(['login']);
  }

}
